#include <rip/routines/template_routine.hpp>

namespace rip::routines
{
    
    TemplateRoutine::TemplateRoutine(const nlohmann::json &config, std::shared_ptr<EventSystem> es, 
            std::string id, std::shared_ptr<std::unordered_map<std::string, 
                std::shared_ptr<RobotComponent> > > comps)
    : Routine(config, es, id, comps)
    {}

    void TemplateRoutine::start(std::vector<std::any> data)
    {}

    void TemplateRoutine::stop(std::vector<std::any> data)
    {}

    void TemplateRoutine::saveComponents(std::shared_ptr<std::unordered_map<std::string, 
            std::shared_ptr<RobotComponent> > > comps)
    {}

    void TemplateRoutine::run()
    {}

}
